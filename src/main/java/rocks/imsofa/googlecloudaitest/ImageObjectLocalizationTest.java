/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package rocks.imsofa.googlecloudaitest;

import com.google.cloud.vision.v1.AnnotateImageRequest;
import com.google.cloud.vision.v1.AnnotateImageResponse;
import com.google.cloud.vision.v1.BatchAnnotateImagesResponse;
import com.google.cloud.vision.v1.EntityAnnotation;
import com.google.cloud.vision.v1.Feature;
import com.google.cloud.vision.v1.Feature.Type;
import com.google.cloud.vision.v1.Image;
import com.google.cloud.vision.v1.ImageAnnotatorClient;
import com.google.cloud.vision.v1.LocalizedObjectAnnotation;
import com.google.protobuf.ByteString;
import java.io.File;
import java.util.List;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author lendle
 */
public class ImageObjectLocalizationTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws Exception {
        // TODO code application logic here
        // Initialize client that will be used to send requests. This client only needs to be created
        // once, and can be reused for multiple requests. After completing all of your requests, call
        // the "close" method on the client to safely clean up any remaining background resources.
        try (ImageAnnotatorClient vision = ImageAnnotatorClient.create()) {

            // The path to the image file to annotate
            String fileName = "./444696013870d541e47d90be2e0f94fc518c804b.webp";

            // Reads the image file into memory
            byte [] data=FileUtils.readFileToByteArray(new File(fileName));
            ByteString imgBytes = ByteString.copyFrom(data);
            Image img = Image.newBuilder().setContent(imgBytes).build();
            
            Feature feature=Feature.newBuilder().setType(Type.OBJECT_LOCALIZATION).build();
            AnnotateImageRequest.Builder builder=AnnotateImageRequest.newBuilder();
            builder.addFeatures(feature);
            builder.setImage(img);
            AnnotateImageRequest request=builder.build();
            BatchAnnotateImagesResponse response=vision.batchAnnotateImages(List.of(request));
            List<AnnotateImageResponse> responses = response.getResponsesList();
            for (AnnotateImageResponse res : responses) {
                List<LocalizedObjectAnnotation> list=res.getLocalizedObjectAnnotationsList();
                for(LocalizedObjectAnnotation annotation : list){
                    System.out.println(annotation.getName()+":"+annotation.getBoundingPoly());
                }
            }
        }
    }

}
